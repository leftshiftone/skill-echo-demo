from unittest import TestCase

from src.echo_service import EchoService


class EchoServiceTest(TestCase):
    """
    This test represents a very fast test. Every test below the test/ directory will be executed before deployment.
    Thus, only fast tests i.e. unit tests should be located under test/.
    """

    def test_transform_to_lowercase(self):
        result = EchoService()._transform("SOMETHING")

        self.assertEqual("something", result)

    def test_transform_to_uppercase(self):
        result = EchoService("uppercase")._transform("someTHING")

        self.assertEqual("SOMETHING", result)

    def test_transform_throws_exception(self):
        echo_service = EchoService("invalid")

        with self.assertRaises(ValueError) as context:
            echo_service._transform("any")
        self.assertEqual("Mode invalid is not supported", str(context.exception))
