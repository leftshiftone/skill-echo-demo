from unittest import TestCase

from src import handler


class IntegrationHandlerTest(TestCase):
    """
    This test represents a slow test. Every test, which tends to be slow or accessing external resources should go here.
    Tests under test_integration/ won't be executed during deployment.
    """

    def test_evaluate(self):
        payload: dict = {"request": "SOMETHING"}
        context: dict = {"owner": "leftshiftone", "name": "echo-test"}

        result = handler.evaluate(payload, context)

        self.assertEqual('{"text": "something"}', result["response"])
