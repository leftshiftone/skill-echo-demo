"""
Skill implementation of spacy named entity extraction.
The spacy model can be configured by the env property SPACY_MODEL.
"""
from dataclasses import asdict

from src.echo_service import EchoService
from src.model import Request, Response

echo_service: EchoService = EchoService()


# Skill calls will begin at the evaluate() method, with the skill payload passed as 'payload'
# and the skill context passed as 'context'.
def evaluate(payload: dict, context: dict) -> dict:
    print(payload)
    print(context)

    request: Request = Request(payload["request"])
    response: Response = echo_service.echo(request)

    return asdict(response)


def on_started(context: dict):
    print("on_started triggered!")


def on_stopped(context: dict):
    print("on_stopped triggered!")
