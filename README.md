# Echo Demo

This [skill](https://www.leftshiftone.com/en/solutions/#skills) represents a demo, which requests data from a HTTP API, transforms the result and returns it.

![Alt text](skill-echo-demo-architecture.svg)

The following aspects are shown in this demo:

* Contract
* Model for contract
* Service module
* Tests
* Integration tests
